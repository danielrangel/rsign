FROM rust:bookworm
COPY ./ /app
WORKDIR /app
#RUN apt update && apt install libsodium-dev build-essential pkg-config -y

# RUN SODIUM_STATIC=yes SODIUM_LIB_DIR=/app/lib cargo test -- --test-threads=1 
# RUN SODIUM_STATIC=yes SODIUM_LIB_DIR=/app/lib cargo build --release --verbose
RUN cargo build --release 

