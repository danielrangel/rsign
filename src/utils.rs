use crate::ErrorKind;
use crate::PError;
use crate::Result;
use dryoc::generichash::{GenericHash, Key};
use std::fmt::Debug;
use std::fs::{DirBuilder, File, OpenOptions};
use std::io::{self, BufRead, BufReader, BufWriter, Read, Write};

use base64::engine::general_purpose::STANDARD as BASE64;
use base64::engine::Engine as _;

use crate::*;

#[cfg(not(windows))]
use std::os::unix::fs::OpenOptionsExt;
use std::path::Path;
use std::str::FromStr;

pub fn create_dir<P>(path: P) -> Result<()>
where
    P: AsRef<Path> + Debug,
{
    DirBuilder::new()
        .recursive(true)
        .create(&path)
        .map_err(|e| PError::new(ErrorKind::Io, format!("while creating: {:?} - {}", path, e)))
        .and_then(|_| Ok(()))
}
#[cfg(windows)]
pub fn create_file<P>(path: P, _mode: u32) -> Result<BufWriter<File>>
where
    P: AsRef<Path> + Copy + Debug,
{
    OpenOptions::new()
        .write(true)
        .create_new(true)
        .open(path)
        .map_err(|e| PError::new(ErrorKind::Io, format!("while creating: {:?} - {}", path, e)))
        .and_then(|file| Ok(BufWriter::new(file)))
}

#[cfg(not(windows))]
pub fn create_file<P>(path: P, mode: u32) -> Result<BufWriter<File>>
where
    P: AsRef<Path> + Copy + Debug,
{
    OpenOptions::new()
        .mode(mode)
        .write(true)
        .create_new(true)
        .open(path)
        .map_err(|e| PError::new(ErrorKind::Io, format!("while creating: {:?} - {}", path, e)))
        .and_then(|file| Ok(BufWriter::new(file)))
}

pub fn create_sig_file<P>(path: P) -> Result<BufWriter<File>>
where
    P: AsRef<Path> + Copy + Debug,
{
    OpenOptions::new()
        .write(true)
        .create(true)
        .open(path)
        .map_err(|e| PError::new(ErrorKind::Io, format!("while creating: {:?} - {}", path, e)))
        .and_then(|file| Ok(BufWriter::new(file)))
}

pub fn sk_load<P: AsRef<Path>>(sk_path: P) -> Result<SeckeyStruct> {
    let mut sk_str = OpenOptions::new()
        .read(true)
        .open(sk_path)
        .map_err(|e| PError::new(ErrorKind::Io, e))
        .and_then(|file| {
            let mut sk_buf = BufReader::new(file);
            let mut _comment = String::new();
            sk_buf.read_line(&mut _comment)?;
            let mut encoded_buf = String::new();
            sk_buf.read_line(&mut encoded_buf)?;
            BASE64
                .decode(encoded_buf.trim())
                .map_err(|e| PError::new(ErrorKind::Io, e))
                .and_then(|decoded_buf| SeckeyStruct::from(&decoded_buf[..]))
        })?;

    let pwd = get_password("Password: ")?;
    write!(
        io::stdout(),
        "Deriving a key from the password and decrypting the secret key... "
    )
    .map_err(|e| PError::new(ErrorKind::Io, e))
    .and_then(|_| {
        io::stdout().flush()?;
        derive_and_crypt(&mut sk_str, &pwd.as_bytes())
    })
    .and(writeln!(io::stdout(), "done").map_err(|e| PError::new(ErrorKind::Io, e)))?;
    sk_str
        .read_checksum()
        .map_err(|e| From::from(e))
        .and_then(|checksum_vec| {
            let mut chk = StackByteArray::new();
            chk.copy_from_slice(&checksum_vec);
            if chk != sk_str.keynum_sk.chk {
                Err(PError::new(
                    ErrorKind::Verify,
                    "Wrong password for that key",
                ))
            } else {
                Ok(sk_str)
            }
        })
}

pub fn pk_load<P>(pk_path: P) -> Result<PubkeyStruct>
where
    P: AsRef<Path> + Copy + Debug,
{
    let pk = OpenOptions::new()
        .read(true)
        .open(pk_path)
        .map_err(|e| {
            PError::new(
                ErrorKind::Io,
                format!("couldn't retrieve public key from {:?}: {}", pk_path, e),
            )
        })
        .and_then(|file| {
            let mut pk_buf = BufReader::new(file);
            let mut _comment = String::new();
            pk_buf.read_line(&mut _comment)?;
            let mut encoded_buf = String::new();
            pk_buf.read_line(&mut encoded_buf)?;
            if encoded_buf.trim().len() != PK_B64_ENCODED_LEN {
                return Err(PError::new(
                    ErrorKind::Io,
                    format!(
                        "base64 conversion failed - was an actual \
                                                public key given?"
                    ),
                ));
            }
            BASE64
                .decode(encoded_buf.trim())
                .map_err(|e| {
                    PError::new(
                        ErrorKind::Io,
                        format!(
                            "base64 conversion failed -
                            was an actual public key given?: {}",
                            e
                        ),
                    )
                })
                .and_then(|decoded_buf| PubkeyStruct::from(&decoded_buf))
        })?;
    Ok(pk)
}

pub fn pk_load_string(pk_string: &str) -> Result<PubkeyStruct> {
    let pk = String::from_str(pk_string)
        .map_err(|e| PError::new(ErrorKind::Io, e))
        .and_then(|encoded_string| {
            if encoded_string.trim().len() != PK_B64_ENCODED_LEN {
                return Err(PError::new(
                    ErrorKind::Io,
                    format!(
                        "base64 conversion failed -
                 was an actual public key given?"
                    ),
                ));
            }
            BASE64
                .decode(encoded_string.as_bytes())
                .map_err(|e| {
                    PError::new(
                        ErrorKind::Io,
                        format!(
                            "base64 conversion
                          failed - was an actual public key given?: {}",
                            e
                        ),
                    )
                })
                .and_then(|decoded_string| PubkeyStruct::from(&decoded_string))
        })?;
    Ok(pk)
}

pub fn sig_load<P>(
    sig_file: P,
    global_sig: &mut Vec<u8>,
    trusted_comment: &mut Vec<u8>,
    hashed: &mut bool,
) -> Result<SigStruct>
where
    P: AsRef<Path> + Copy + Debug,
{
    File::open(sig_file)
        .map_err(|e| PError::new(ErrorKind::Io, format!("{} {:?}", e, sig_file)))
        .and_then(|file| {
            let mut buf = BufReader::new(file);
            let mut untrusted_comment = String::with_capacity(COMMENTBYTES);
            buf.read_line(&mut untrusted_comment)
                .map_err(|e| PError::new(ErrorKind::Io, e))
                .and_then(|_| {
                    let mut sig_string = String::with_capacity(SigStruct::len());
                    buf.read_line(&mut sig_string)
                        .map_err(|e| PError::new(ErrorKind::Io, e))
                        .and_then(|_| {
                            let mut t_comment = String::with_capacity(TRUSTEDCOMMENTMAXBYTES);
                            buf.read_line(&mut t_comment)
                                .map_err(|e| PError::new(ErrorKind::Io, e))
                                .and_then(|_| {
                                    let mut g_sig = String::with_capacity(SIGNATUREBYTES);
                                    buf.read_line(&mut g_sig)
                                        .map_err(|e| PError::new(ErrorKind::Io, e))
                                        .and_then(|_| {
                                            if !untrusted_comment.starts_with(COMMENT_PREFIX) {
                                                return Err(PError::new(ErrorKind::Verify,
                                                                       format!("Untrusted comment must start with: {}", COMMENT_PREFIX)));
                                            }
                                            BASE64.decode(sig_string.trim().as_bytes())
                                                .map_err(|e| PError::new(ErrorKind::Io, e))
                                                .and_then(|sig_bytes| {
                                                    SigStruct::from(&sig_bytes).and_then(|sig| {
                                                        if !t_comment.starts_with(TRUSTED_COMMENT_PREFIX) {
                                                            return Err(PError::new(ErrorKind::Verify,
                                                                                   format!("trusted comment should start with: {}",
                                                                                           TRUSTED_COMMENT_PREFIX)));
                                                        }
                                                        if sig.sig_alg == SIGALG {
                                                            *hashed = false;
                                                        } else if sig.sig_alg == SIGALG_HASHED {
                                                            *hashed = true;
                                                        } else {
                                                            return Err(PError::new(ErrorKind::Verify,
                                                                                   format!("Unsupported signature algorithm")));
                                                        }
                                                        let _ = t_comment.drain(..TR_COMMENT_PREFIX_LEN).count();
                                                        trusted_comment.extend(sig.sig.iter());
                                                        trusted_comment.extend_from_slice(t_comment.trim().as_bytes());
                                                        BASE64.decode(g_sig.trim().as_bytes())
                                                            .map_err(|e| PError::new(ErrorKind::Io, e))
                                                            .and_then(|comm_sig| {
                                                                          global_sig.extend_from_slice(&comm_sig);
                                                                          Ok(sig)
                                                                      })
                                                    })
                                                })

                                        })
                                })
                        })
                })
        })
}

pub fn load_message_file<P>(file_path: P, hashed: bool) -> Result<Vec<u8>>
where
    P: AsRef<Path> + Copy + Debug,
{
    if hashed {
        return hash_message_file(file_path);
    }
    let mut file = File::open(file_path)?;

    if file.metadata()?.len() > (1u64 << 30) {
        return Err(PError::new(
            ErrorKind::Io,
            format!("{:?} is larger than 1G try using -H", file_path),
        ));
    }
    let mut msg_buf: Vec<u8> = Vec::new();
    file.read_to_end(&mut msg_buf)?;
    Ok(msg_buf)
}

fn hash_message_file<P>(message_file: P) -> Result<Vec<u8>>
where
    P: AsRef<Path> + Copy,
{
    let f = File::open(message_file)?;
    let mut buf_reader = BufReader::new(f);
    let mut buf_chunk = [0u8; 65536];
    let mut hasher = GenericHash::new_with_defaults::<Key>(None)?;
    while buf_reader.read(&mut buf_chunk).unwrap() > 0 {
        hasher.update(&buf_chunk);
    }

    let hash = hasher.finalize_to_vec()?;

    Ok(hash)
}
