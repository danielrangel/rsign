extern crate base64;
extern crate chrono;
extern crate clap;
extern crate rsign;
extern crate sodiumoxide;

use chrono::prelude::*;

use homedir::my_home;

use sodiumoxide::crypto::sign::SIGNATUREBYTES;

use std::path::PathBuf;

use rsign::*;

use crate::rsign::utils::*;

use base64::engine::general_purpose::STANDARD as BASE64;
use base64::engine::Engine as _;

fn run<'a>(args: clap::ArgMatches<'a>) -> Result<()> {
    if let Some(generate_action) = args.subcommand_matches("generate") {
        let force = generate_action.is_present("force");
        let pk_path = match generate_action.value_of("pk_path") {
            Some(path) => PathBuf::from(path),
            None => PathBuf::from(SIG_DEFAULT_PKFILE),
        };
        if pk_path.exists() {
            if !force {
                return Err(PError::new(
                    ErrorKind::Io,
                    format!(
                        "Key generation aborted:\n
                {:?} already exists\n
                If you really want to overwrite the existing key pair, add the -f switch to\n
                force this operation.",
                        pk_path
                    ),
                ));
            } else {
                std::fs::remove_file(&pk_path)?
            }
        }

        let sk_path = match generate_action.value_of("sk_path") {
            Some(path) => {
                let complete_path = PathBuf::from(path);
                let mut dir = complete_path.clone();
                dir.pop();
                create_dir(dir)?;
                complete_path
            }
            None => {
                let env_path = std::env::var(SIG_DEFAULT_CONFIG_DIR_ENV_VAR);
                let path = match env_path {
                    Ok(env_path) => {
                        let mut complete_path = PathBuf::from(env_path);
                        if !complete_path.exists() {
                            return Err(PError::new(
                                ErrorKind::Io,
                                format!(
                                    "folder {:?} referenced by {} \
                                                            doesn't exists,
                                     \
                                                            you'll have to create yourself",
                                    complete_path, SIG_DEFAULT_CONFIG_DIR_ENV_VAR
                                ),
                            ));
                        }
                        complete_path.push(SIG_DEFAULT_SKFILE);
                        complete_path
                    }
                    Err(_) => {
                        let home_path = my_home()?; // my_home().ok_or(PError::new(ErrorKind::Io,"can't find home dir"));
                        let mut complete_path = PathBuf::from(home_path.unwrap());
                        complete_path.push(SIG_DEFAULT_CONFIG_DIR);
                        if !complete_path.exists() {
                            create_dir(&complete_path)?
                        }
                        complete_path.push(SIG_DEFAULT_SKFILE);
                        complete_path
                    }
                };
                path
            }
        };

        if sk_path.exists() {
            if !force {
                return Err(PError::new(
                    ErrorKind::Io,
                    format!(
                        "Key generation aborted:
{:?} already exists

If you really want to overwrite the existing key pair, add the -f switch to
force this operation.",
                        sk_path
                    ),
                ));
            } else {
                std::fs::remove_file(&sk_path)?;
            }
        }
        let pk_file = create_file(&pk_path, 0o644)?;
        let sk_file = create_file(&sk_path, 0o600)?;
        let (pk_str, _) = generate(pk_file, sk_file, generate_action.value_of("comment"))?;

        println!(
            "\nThe secret key was saved as {:?} - Keep it secret!",
            sk_path
        );
        println!(
            "The public key was saved as {:?} - That one can be public.\n",
            pk_path
        );
        println!("Files signed using this key pair can be verified with the following command:\n");
        println!(
            "rsign verify <file> -P {}",
            BASE64.encode(pk_str.bytes().as_slice())
        );
    } else if let Some(sign_action) = args.subcommand_matches("sign") {
        let sk_path = match sign_action.value_of("sk_path") {
            Some(path) => PathBuf::from(path),
            None => {
                let home_path = my_home()?;
                let mut complete_path = PathBuf::from(home_path.unwrap());
                complete_path.push(SIG_DEFAULT_CONFIG_DIR);
                complete_path.push(SIG_DEFAULT_SKFILE);
                complete_path
            }
        };
        if !sk_path.exists() {
            return Err(PError::new(
                ErrorKind::Io,
                format!("can't find secret key file at {:?}, try using -s", sk_path),
            ));
        }

        let mut pk: Option<PubkeyStruct> = None;
        if sign_action.is_present("pk_path") {
            if let Some(filename) = sign_action.value_of("pk_path") {
                pk = Some(pk_load(filename)?);
            }
        } else if sign_action.is_present("public_key") {
            if let Some(string) = sign_action.value_of("public_key") {
                pk = Some(pk_load_string(string)?);
            }
        }
        let is_hashed = sign_action.is_present("hash");
        let message_file = sign_action.value_of("message").unwrap(); //TODO: manage this error

        let sig_file_name = if let Some(file) = sign_action.value_of("sig_file") {
            format!("{}", file)
        } else {
            format!("{}{}", message_file, SIG_SUFFIX)
        };
        let sig_buf = create_sig_file(&sig_file_name)?;

        let sk = sk_load(sk_path)?;

        let t_comment = if let Some(trusted_comment) = sign_action.value_of("trusted-comment") {
            format!("{}", trusted_comment)
        } else {
            format!(
                "timestamp:{}\tfile:{}",
                Utc::now().timestamp(),
                message_file
            )
        };

        let unt_comment = if let Some(untrusted_comment) = sign_action.value_of("untrusted-comment")
        {
            format!("{}{}", COMMENT_PREFIX, untrusted_comment)
        } else {
            format!("{}{}", COMMENT_PREFIX, DEFAULT_COMMENT)
        };
        let message = load_message_file(message_file, is_hashed)?;

        sign(
            sk,
            pk,
            sig_buf,
            message.as_ref(),
            is_hashed,
            t_comment.as_str(),
            unt_comment.as_str(),
        )?;
    } else if let Some(verify_action) = args.subcommand_matches("verify") {
        let input = verify_action
            .value_of("pk_path")
            .or(verify_action.value_of("public_key"));

        let pk = match input {
            Some(path_or_string) => {
                if verify_action.is_present("pk_path") {
                    pk_load(path_or_string)?
                } else {
                    pk_load_string(path_or_string)?
                }
            }
            None => pk_load(SIG_DEFAULT_PKFILE)?,
        };

        let message_file = verify_action.value_of("file").unwrap(); //safe to unwrap

        let sig_file_name = if let Some(file) = verify_action.value_of("sig_file") {
            format!("{}", file)
        } else {
            format!("{}{}", message_file, SIG_SUFFIX)
        };
        let mut is_hashed: bool = false;

        let mut trusted_comment: Vec<u8> = Vec::with_capacity(TRUSTEDCOMMENTMAXBYTES);
        let mut global_sig: Vec<u8> = Vec::with_capacity(SIGNATUREBYTES);
        let sig = sig_load(
            sig_file_name.as_str(),
            &mut global_sig,
            &mut trusted_comment,
            &mut is_hashed,
        )?;

        let message = load_message_file(message_file, is_hashed)?;

        verify(
            pk,
            sig,
            &global_sig[..],
            trusted_comment.as_ref(),
            message.as_ref(),
            verify_action.is_present("quiet"),
            verify_action.is_present("output"),
        )?;
    } else {
        println!("{}\n", args.usage());
    }

    Ok(())
}

fn main() {
    let args = parse_args();
    run(args).map_err(|e| e.exit()).unwrap();
    std::process::exit(0);
}

#[test]
fn load_public_key_string() {
    assert!(pk_load_string("RWRzq51bKcS8oJvZ4xEm+nRvGYPdsNRD3ciFPu1YJEL8Bl/3daWaj72r").is_ok());
    assert!(pk_load_string("RWQt7oYqpar/yePp+nonossdnononovlOSkkckMMfvHuGc+0+oShmJyN5Y").is_err());
}
